# coding: UTF-8

from pprint import pprint
from collections import defaultdict

def fmtset(syms):
    return ' '.join('$' if s is None else
                    'ε' if s == '' else
                     str(s) for s in syms) or '∅'

def fmtlist(syms):
    syms = ' '.join('$' if s is None else
                     str(s) for s in syms).strip()
    return syms.strip() or 'ε'

grammar = [
    ('S`', ('S',)),
    ('S', ('A',)),
    ('S', ('x', 'b',)),
    ('A', ('a', 'A', 'b',)),
    ('A', ('B',)),
    ('B', ('x',)),
]
print('Грамматика:')
for i, (lhs, rhs) in enumerate(grammar):
    print(' ({}) '.format(i), lhs, '->', fmtlist(rhs))
print()


variables = set()
for lhs, rhs in grammar:
    variables |= {lhs}

terminals = set()
for lhs, rhs in grammar:
    for symbol in rhs:
        if symbol not in variables:
            terminals |= {symbol}


first1 = {'': {''}}
for terminal in terminals:
    first1[terminal] = {terminal}
for variable in variables:
    first1[variable] = {'' for lhs, rhs in grammar
                           if lhs == variable and rhs == ()}
def first1_(alpha):
    result = set()
    for symbol in alpha:
        result |= first1[symbol] - {''}
        if '' not in first1[symbol]:
            break
    if all('' in first1[s] for s in alpha):
        result |= {''}
    return result
while True:
    old_first1 = {s: fs.copy() for s, fs in first1.items()}
    for lhs, rhs in grammar:
        first1[lhs] |= first1_(rhs)
    if first1 == old_first1:
        break
#print('Множества FIRST₁ sets:')
#for v in variables:
#    print('  FIRST₁({}) = {}'.format(v, fmtset(first1[v])))
#print()


follow1 = {v: ({None} if v == grammar[0][0] else set()) for v in variables}
while True:
    old_follow1 = {s: fs.copy() for s, fs in follow1.items()}
    for lhs, rhs in grammar:
        for i, symbol in enumerate(rhs):
            if symbol not in variables:
                continue
            follow1[symbol] |= first1_(rhs[i + 1:]) - {''}
            if '' in first1_(rhs[i + 1:]):
                follow1[symbol] |= follow1[lhs]
    if follow1 == old_follow1:
        break
#print('Множества FOLLOW₁:')
#for v in variables:
#    print('  FOLLOW₁({}) = {}'.format(v, fmtset(follow1[v])))
#print()


def compute_closure(state):
    state = set(state)
    while True:
        old_state = state.copy()
        for i, p in old_state:
            _, rhs = grammar[i]
            if p == len(rhs):
                continue
            for j, (lhs, _) in enumerate(grammar):
                if lhs == rhs[p]:
                    state.add((j, 0))
        if state == old_state:
            break

    return frozenset(state)

def compute_target(origin, symbol):
    target = set()
    for i, p in origin:
        _, rhs = grammar[i]
        if p == len(rhs):
            continue
        if rhs[p] == symbol:
            target.add((i, p + 1))

    return compute_closure(target)

states = [compute_closure({(0, 0)})]
for state in states:
    for symbol in terminals | variables:
        target_state = compute_target(state, symbol)
        if not target_state:
            continue
        if target_state in states:
            continue
        states.append(target_state)

actions = defaultdict(set)
for i, state in enumerate(states):
    for r, p in state:
        lhs, rhs = grammar[r]
        if p == len(rhs):
            if r == 0:
                actions[i, None].add(('a', None))
            else:
                for t in follow1[lhs]:
                    actions[i, t].add(('r', r))
        elif rhs[p] in terminals:
            actions[i, rhs[p]].add(('s', states.index(compute_target(state, rhs[p]))))
for k in actions:
    actions[k] = sorted(actions[k], reverse=True)

transitions = {}
for i, state in enumerate(states):
    for v in variables:
        try:
            transitions[i, v] = states.index(compute_target(state, v))
        except ValueError:
            pass

def fmtacts(state, terminal):
    return '/'.join(a + str(d or '') for a, d in actions[state, terminal])

def fmttrans(state, variable):
    try:
        return str(transitions[state, variable])
    except KeyError:
        return ''

print('Управляющая таблица:')
print('       ', '│', ('{:^' + str((len(terminals) + 1) * 7) + '}').format('ACTION'),
                 '│', ('{:^' + str(len(variables) * 7) + '}').format('GOTO'), sep='')
print(' state ', '│', ''.join(map(' {:^5} '.format, [*terminals, '$'])),
                 '│', ''.join(map(' {:^5} '.format, variables)), sep='')
print('───────', '┼', '─' * ((len(terminals) + 1) * 7),
                 '┼', '─' * (len(variables) * 7), sep='')
for s in range(len(states)):
    print(' {:^5} '.format(s), '│', ''.join(map(' {:^5} '.format, (fmtacts(s, t) for t in [*terminals, None]))),
                               '│', ''.join(map(' {:^5} '.format, (fmttrans(s, v) for v in variables))), sep='')
print()


input = tuple('aaaxbbb')
print('Входная цепочка:')
print('', fmtlist(input))
print()

states = (0,)
symbols = ()
input = (*input, None)
action = None
trace = []
while True:
    action, data = actions[states[-1], input[0]][0]
    trace.append((states, symbols, input, (action, data)))
    if action == 's':
        states = (*states, data)
        symbols = (*symbols, input[0])
        input = input[1:]
    elif action == 'r':
        lhs, rhs = grammar[data]
        states = (*states[:-len(rhs)], transitions[states[-len(rhs)-1], lhs])
        symbols = (*symbols[:-len(rhs)], lhs)
    elif action == 'a':
        break
    else:
        assert False

states_width = len('states')
symbols_width = len('symbols')
input_width = len('input')
action_width = len('action')
for i, (states, symbols, input, action) in enumerate(trace):
    states = ' '.join(map(str, states))
    symbols = ' '.join(map(lambda s: '$' if s is None else s, symbols))
    input = ' '.join(map(lambda s: '$' if s is None else s, input))
    action = action[0] + ('' if action[1] is None else str(action[1]))

    states_width = max(states_width, len(states))
    symbols_width = max(symbols_width, len(symbols))
    input_width = max(input_width, len(input))
    action_width = max(action_width, len(action))

    trace[i] = states, symbols, input, action

print('Разбор:')
print(' {} │ {} │ {} │ {} '.format(
    ('{:^' + str(states_width) + '}').format('states'),
    ('{:^' + str(symbols_width) + '}').format('symbols'),
    ('{:^' + str(input_width) + '}').format('input'),
    ('{:^' + str(action_width) + '}').format('action'),
))
print('{}┼{}┼{}┼{}'.format(
    '─'*(states_width + 2),
    '─'*(symbols_width + 2),
    '─'*(input_width + 2),
    '─'*(action_width + 2),
))
for states, symbols, input, action in trace:
    print(' {} │ {} │ {} │ {} '.format(
        ('{:<' + str(states_width) + '}').format(states),
        ('{:<' + str(symbols_width) + '}').format(symbols),
        ('{:>' + str(input_width) + '}').format(input),
        ('{:^' + str(action_width) + '}').format(action),
    ))




